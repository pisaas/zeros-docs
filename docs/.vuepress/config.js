module.exports = {
  port: '5000',
  dest: 'public',
  base: '/zeros-docs/',
  title: 'Zeros',
  head: [['link', { rel: 'icon', href: '/logo.png' }]],
  themeConfig: {
    logo: '/logo.png',
    nav: [
      { text: '首页', link: '/' },
      { text: 'FAQ', link: '/faq/' },
      { text: '热点', link: '/trend/' },
    ],
    sidebar: {
      '/trend/': [
        { title: '人工智能', path: '/trend/ai/', collapsable: false,
          children: [
            { title: 'TensorFlow', path: '/trend/ai/tensor-flow.md' }
          ]
        },
      ],
      '/': [
        { title: '用户指南', path: '/guide/', collapsable: false },
        { title: '应用部署', path: '/ops/', collapsable: false,
          children: [
            { title: 'GitLab', path: '/ops/gitlab.md' },
            { title: 'Docker', path: '/ops/docker.md' },
            { title: 'Kubernetes', path: '/ops/k8s.md' },
            { title: 'Redis', path: '/ops/redis.md' },
            { title: 'Nginx', path: '/ops/nginx.md' },
            { title: 'Mongo', path: '/ops/mongo.md' },
            { title: 'Ansible', path: '/ops/ansible.md' },
            { title: 'NPM私有库', path: '/ops/npm.md' },
          ]
        },
        { title: '开发指南', path: '/dev/', collapsable: false },
        { title: 'Q&A', path: '/faq/', collapsable: false },
      ]
    }
  }
}