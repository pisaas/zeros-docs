# Q&A

## 为什么使用Node.js
我们曾犹豫在选择Node.js还是Golang。在经过一系列对比后我们依据如下原则，选择了Node.js。

- 在项目初期我们必须快速的构建应用让产品以更低的成本和更高的开发效率。Node.js作为在之前几年广为推崇的技术积累的庞大的开发社区和开源库。我们很容易在此基础上找到任何问题的解决方案。
- 我们应用的特色主要还是面向社交，我们将有大量的业务基于即时通讯、视频聊天，而Node.js基于事件的模式非常好的适用于此业务场景。
- Node.js和前端开发采用非常类似的开发模式，这将减少我们在不同技术间切换的成本。比如部分库可以在前后端共用或构建可以更好兼容前后端的库，实际上我们使用feathersjs构建了实时应用解决方案。
- 由于我们团队在Node.js和Javascript的开发方面积累了非常丰富的经验。Node.js可以帮助我们快速的构建应用。

在性能要求很高以及高并发，这些Golang的优势领域，我们会考虑使用Go。比如我们会考虑使用Go来构建定时任务等小要求比较的服务。

虽然高并发也可以通过基础设施的完善来解决，比如容器的技术等。但在项目初期，这暂时不是我们的选项。

以下是我们参考的一些Golang和Node.js的对比：
- [Golang vs. Node.js — Comparison and Why Developers prefer Node.js](https://medium.com/@blogger.ashishsharma/golang-vs-node-js-comparison-and-why-developers-prefer-node-js-9e669319df52)
- [NodeJs vs Golang: Which One Leads in Backend Development?](https://www.intellectsoft.net/blog/nodejs-vs-golang/)

另外，程序员们也可以课外阅读下这个：
+ [Stackoverflow Survey 2019](https://insights.stackoverflow.com/survey/2019)