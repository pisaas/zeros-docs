--- 
home: true
heroText: Zeros
tagline: 让我们一起，从起点开始
actionText: 出发 →
actionLink: /guide/

features:
- title: 用户指南
  details: Zeros后台控制台，管理后台等使用指导，包括公众号、小程序、页面、资源、流程等配置
- title: 应用部署
  details: Zeros部署文档，包括：npm私有库，mongodb, redis, nginx等完整的过程搭建文档
- title: 开发指南
  details: Zeros将提供通常应用构建需要的通用功能，让我们更快的进行应用开发
footer: Copyright © 2020 - Pistech Present 
--- 

<!-- ## CI/CD
- [创建私有NPM库](/ci/npm.md).  -->