# nginx运维

## 安装部署
### yum 安装
```bash
sudo yum install -y epel-release

sudo yum install -y nginx

sudo chkconfig nginx on # 设置服务开机启动nginx

# 启动nginx服务
# nginx 启动服务时CentOS 6.X下可能报错：
# Starting nginx: nginx: [emerg] socket() [::]:80 failed (97: Address family not supported by protocol)
# 解决办法:
#   vim /etc/nginx/conf.d/default.conf
#   注释： # listen       [::]:80 default_server;
sudo service nginx start
```

### 手工安装
```
cd /opt/pkgs
wget http://nginx.org/download/nginx-1.12.2.tar.gz
tar zxvf ./nginx-1.12.2.tar.gz
cd nginx-1.12.2

./configure --prefix=/opt/nginx --with-http_ssl_module

sudo ln -s /opt/nginx/sbin/nginx /usr/local/bin
sudo ln -sf /opt/nginx/conf/ /etc/nginx
```

## nginx配置
```
// 备份原配置
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf_bak
sudo vim /etc/nginx/nginx.conf
```

配置内容

```
# gzip config
gzip on;
gzip_min_length 1k;
gzip_buffers 4 16k;
# gzip_http_version 1.1;
gzip_comp_level 2;
gzip_types text/plain application/javascript text/css
gzip_vary on;
gzip_disable "MSIE [1-6]\.";

# prefare for websocket config
map $http_upgrade $connection_upgrade {
    default   upgrade;
    ''        close;
}

# h5 site config
server {
    listen       80 default_server;
    # listen       [::]:80 default_server;
    server_name  _;
    # root         /usr/share/nginx/html;
    root         /opt/apps/daiyu/app-h5/dist_pub;

    # Load configuration files for the default server block.
    include /etc/nginx/default.d/*.conf;

    location / {
    }

    location ~ (/api|/socket.io|/p|/resources|/login|/policies|/guide) {
        proxy_pass  http://127.0.0.1:1337;
        client_max_body_size 1m;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
    }

    error_page 404 /40x.html;
        location = /40x.html {
        root    /opt/apps/daiyu/ops/pages/errors/40x.html;
    }

    error_page 500 502 503 504 /50x.html;
        location =/50x.html {
        root    /opt/apps/daiyu/ops/pages/errors/50x.html;
    }
}

server {
    listen       80;
    server_name  mp_admin_1.yuedupai.net;
    root         /opt/apps/daiyu/web-adm/dist_pub;

    location / {
    }

    #location ~ (/api/admin) {
    #    proxy_pass  http://127.0.0.1:1337;
    #    client_max_body_size 1m;
    #}

    location /api/ {
        proxy_pass  http://127.0.0.1:1337/api/admin/;
        client_max_body_size 1m;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    error_page 404 /404.html;
        location = /40x.html {
        root    /opt/apps/daiyu/ops/pages/errors/40x.html;
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        root    /opt/apps/daiyu/ops/pages/errors/50x.html;
    }
}
```
重新加载
```
sudo nginx -s reload
```

## 安装certbot
安装certbot前需要先配置好网站
```
# Let's Encrypt
# 参考：https://github.com/letsencrypt/letsencrypt
sudo yum install python2-certbot-nginx

sudo certbot --nginx
# 可能出现如下错误：UnicodeDecodeError: 'ascii' codec can't decode byte 0xe9 in position 0: ordinal not in range(128)
# 检查nginx配置是否有中文或特殊字符, certbot只支持ascii字符

sudo certbot renew --dry-run

```
创建定时更新任务
```bash
# 创建crontab日志目录
mkdir -p /opt/apps/logs/certbot

su root
crontab -e

# 添加如下任务（每月1号凌晨1点执行，不知道为什么无效，后续加入job任务中）
# 0 */12 * * * /usr/bin/certbot renew --dry-run >> /opt/apps/logs/certbot/log_$(date +\%Y-\%m-\%d).log 2>&1
```

## nginx卸载
```
# 停止服务
sudo systemctl stop nginx.service
sudo systemctl disable nginx.service

# 手工移除文件
sudo userdel -r nginx
sudo chkconfig nginx off
sudo rm -rf /etc/nginx
sudo rm -rf /var/log/nginx
sudo rm -rf /var/cache/nginx/
sudo rm -rf /usr/lib/systemd/system/nginx.service
sudo rm /usr/sbin/nginx

# 移除nginx
sudo yum remove nginx
```
---

### VIM的列编辑操作
```
删除列
1.光标定位到要操作的地方。
2.CTRL+v 进入“可视 块”模式，选取这一列操作多少行。
3.d 删除。

插入列
插入操作的话知识稍有区别。例如我们在每一行前都插入"() "：
1.光标定位到要操作的地方。
2.CTRL+v 进入“可视 块”模式，选取这一列操作多少行。
3.SHIFT+i(I) 输入要插入的内容。
4.ESC 按两次，会在每行的选定的区域出现插入的内容。
```

### 移除appnode nginx
```
sudo yum remove appnode-nginx-stable.x86_64
```
