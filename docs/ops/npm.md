# 创建私有NPM库 （使用 Verdaccio）

## 安装 verdaccio
```bash
npm install –global verdaccio

verdaccio
```
作为deamon运行
```bash
npm install forever -g

forever start /usr/local/lib/node_modules/verdaccio/build/lib/cli.js
forever list

# 查看日志
forever logs

# 停止
forever list
forever stop $pid
```

## 配置
```bash
vim /Users/liuting/.config/verdaccio/config.yaml
```
调整配置文件，添加uplink及修改默认proxy为taobao
```yml
uplinks:
  npmjs:
    url: https://registry.npmjs.org/
+  taobao:
+    url: https://registry.npm.taobao.org/

packages:
  '@*/*':
    # scoped packages
    access: $all
    publish: $authenticated
    unpublish: $authenticated
    # proxy: npmjs
+    proxy: taobao
 
    # if package is not available locally, proxy requests to 'npmjs' registry
M    # proxy: npmjs
+    proxy: taobao
 
middlewares:
  audit:
    enabled: true

+ # 监听的端口 ,重点, 不配置这个,只能本机能访问
+ listen: 0.0.0.0:4873
```
## 使用
```bash
# 使当前npm服务指向本地 
npm set registry http://localhost:4873 
# 注册用户 
npm adduser –registry http://localhost:4873
# 设置用户名、密码、Email

#查看当前用户,是否是注册用户. 
npm who am i

# 添加registry到nrm
nrm add local http://localhost:4873/
nrm use local
nrm test local

# 删除 registry
nrm del local
```


