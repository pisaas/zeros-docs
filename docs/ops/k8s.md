# Kubernetes

在CI/CD方案调研过程中，重点研究了K8s，我们发现：
- K8s的流行度和影响力已经远远超过我们之前的估计。
- 并且在用Doker Swarm计划CI/CD时，发现有很多组件仍然需要我们自己寻找方案，比如负载均衡，服务发现，定时任务，存储管理等等，而K8s全部这些全堵包含了。
- 但最终没有选择K8s作为最终方案，是因为：
  - 他实在太复杂，并且太多的组件运行起来消耗大量资源，Swarm运行只需要几十M内存
  - Swarm作为简洁的方案在一段时间内完全满足我们的需求。如果我们的业务发展到Swarm满足不了了，那我们完全可以请一个专业运维，而不是把它交给全栈工程师
- 虽然最终没有选择K8s，但对他的了解为我们部署高可用网络提供重要的参考和思路。

## K8s部署

### 安装部署虚拟机
配置NAT网络注意，需要保持虚拟机之间，虚拟机与本机之间，虚拟机与外网可以联通。

修改网关信息
```bash
# 新建NAT网络 设置网关为192.168.66.2(网关地址不能为1（宿主机使用）， 不能为255)
# Vmware Fusion网络下，需要修改网关信息
vim /Library/Preferences/VMware\ Fusion/networking

# 修改配置文件...
# answer VNET_2_DHCP yes
# answer VNET_2_DHCP_CFG_HASH 1560D311C0D4D3C83B243013A0B4B1EA87A9C3FE
# answer VNET_2_DISPLAY_NAME vmnet NAT
# answer VNET_2_HOSTONLY_NETMASK 255.255.255.0
# answer VNET_2_HOSTONLY_SUBNET 192.168.66.0
# answer VNET_2_NAT yes
# answer VNET_2_NAT_PARAM_UDP_TIMEOUT 30
# answer VNET_2_VIRTUAL_ADAPTER yes

vim /Library/Preferences/VMware\ Fusion/vmnet2/nat.conf
# # NAT gateway address
# ip = 192.168.66.2
# netmask = 255.255.255.0
```

常用命令
```bash
# 查看ip信息
ip addr

# 重启网卡
ifdown ens33
ifup ens33

# 配置主机名以及Host文件的相互解析
hostnamectl set-hostname <hostname>
scp /etc/hosts root@k8s-node01:/etc/hosts
scp /etc/hosts root@k8s-node02:/etc/hosts

# 安装依赖包
yum install -y conntrack ntpdate ntp ipvsadm ipset jq iptables curl sysstat libseccomp wget vim net-tools git

# 设置防火墙为Iptables并设置空规则
systemctl stop firewalld && systemctl disable firewalld
yum -y install iptables-services && systemctl start iptables && systemctl enable iptables 
&& iptables -F && service iptables save

# 关闭交换区，防止容器运行在虚拟内存中
# swapoff -a 然后注释 /etc/fstab最后一行
swapoff -a && sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# 关闭SELINUX
# setenforce 0并设置SELINUX为disabled
setenforce 0 && sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config

# 调整内核参数，对于k8s
cat > kubernetes.conf <<EOF
net.bridge.bridge-nf-call-iptables=1  # *必备，关闭网桥模式
net.bridge.bridge-nf-call-ip6tables=1 # *必备，关闭网桥模式
net.ipv4.ip_forward=1
net.ipv4.tcp_tw_recycle=0
vm.swappiness=0 # 禁止使用swap空间，只有当系统OOM时才允许使用它
vm.overcommit_memory=1 # 不检查物理内存是否够用
vm.panic_on_oom=0 # 开启OOM
fs.inotify.max_user_instances=8192
fs.inotify.max_user_watches=1048576
fs.file-max=52706963
fs.nr_open=52706963
net.ipv6.conf.all.disable_ipv6=1   # *必备，关闭ipv6
net.netfilter.nf_conntrack_max=2310720
EOF
cp kubernetes.conf /etc/sysctl.d/kubernetes.conf
sysctl -p /etc/sysctl.d/kubernetes.conf

# 调整系统时区
# 设置系统分区为上海时间
timedatectl set-timezone Asia/Shanghai
# 将当前的UTC时间写入硬件时钟
timedatectl set-local-rtc 0
# 重启依赖于系统时间的服务
systemctl restart rsyslog
systemctl restart crond

# 关闭系统不需要的服务
systemctl stop postfix && systemctl disable postfix

# 设置rsyslogd和systemd journald
mkdir /var/log/journal # 持久化保存日志的目录
mkdir /etc/systemd/journald.conf.d
cat > /etc/systemd/journald.conf.d/99-prophet.conf <<EOF
[Journal]
# 持久化保存到磁盘
Storage=persistent

# 压缩历史日志
Compress=yes

SyncIntervalSec=5m
RateLimitInterval=30s
RateLimitBurst=1000

# 最大占用空间10G
SystemMaxUse=10G

# 但日志文件最大 200M
SystemMaxFileSize=200M

# 日志保存时间2周
MaxRetentionSec=2week

# 不将日志转发到syslog *重要
ForwardToSyslog=no
EOF

systemctl restart systemd-journald
```

### 升级系统内核为4.44
CentOs 7.x系统自带3.10.x内核存在一些bugs，将导致Docke、Kubernetes不稳定，可以考虑升级内核
```bash
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
# 安装完成后检查/boot/grub2/grub.cfg中对应内核menuentry中是否包含initrd16配置，如果没有，再安装一次
yum --enablerepo=elrepo-kernel install -y kernel-lt

# 设置开机从新内核启动
grub2-set-default "CentOS Linux (4.4.214-1.el7.elrepo.x86_64) 7 (Core)" && reboot


# 查询内核版本
uname -r
```

### kube-proxy开启ipvs的前置条件
```bash
modprobe br_netfilter

cat > /etc/sysconfig/modules/ipvs.modules <<EOF
#!/bin/bash
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack_ipv4
EOF

chmod 755 /etc/sysconfig/modules/ipvs.modules && bash /etc/sysconfig/modules/ipvs.modules && lsmod | grep -e ip_vs -e nf_conntrack_ipv4
```

### 安装Docker相关软件
```bash
# 安装依赖
yum install -y yum-utils device-mapper-persistent-data lvm2

# 倒入阿里云镜像仓库
yum-config-manager \
  --add-repo \
  http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

yum update -y && yum install -y docker-ce

# 安装完成后从新内核重启
grub2-set-default "CentOS Linux (4.4.214-1.el7.elrepo.x86_64) 7 (Core)" && reboot

# 查看历史命令
history

## 启动docker并设置为开机自启
systemctl start docker && systemctl enable docker

## 创建/etc/docker目录
mkdir /etc/docker

# 配置daemon
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  }
}
EOF
mkdir -p /etc/systemd/system/docker.service.d

# 重启docker服务
systemctl daemon-reload && systemctl restart docker && systemctl enable docker
```

### 安装Kubeadm (主从配置)
```bash
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

yum -y install kubeadm-1.15.1 kubectl-1.15.1 kubelet-1.15.1
systemctl enable kubelet.service
```

### 初始化主节点
初始化之前可以先导入镜像

#### 离线安装镜像
```bash
# 查看安装所需镜像
kubeadm config images list

# 解压镜像
tar -zxvf kubeadm-basic.images.tar.gz

# 编辑load-image.sh镜像加载脚本
vim load-image.sh
# 编辑...
chmod a+x load-image.sh
./load-image.sh

scp -r kubeadm-basic.images load-image.sh root@k8s-node01:/root
scp -r kubeadm-basic.images load-image.sh root@k8s-node02:/root

# 在子节点机器执行
./load-image.sh

```

镜像加载脚本内容
```bash
#!/bin/bash

ls /root/kubeadm-basic.images > /tmp/image-list.txt
cd /root/kubeadm-basic.images

for i in $( cat /tmp/image-list.txt )
do
  docker load -i $i
done

rm -rf /tmp/image-list.txt
```

#### 从docker安装镜像

直接转docker安装
```bash
kubeadm config images list |sed -e 's/^/docker pull /g' -e 's#k8s.gcr.io#docker.io/mirrorgooglecontainers#g' |sh -x
docker images |grep mirrorgooglecontainers |awk '{print "docker tag ",$1":"$2,$1":"$2}' |sed -e 's#docker.io/mirrorgooglecontainers#k8s.gcr.io#2' |sh -x
docker images |grep mirrorgooglecontainers |awk '{print "docker rmi ", $1":"$2}' |sh -x
docker pull coredns/coredns:1.2.2
docker tag coredns/coredns:1.2.2 k8s.gcr.io/coredns:1.2.2
docker rmi coredns/coredns:1.2.2
```

保存docker镜像并导入
```bash
kubeadm config images list
docker pull mirrorgooglecontainers <相关镜像>
docker tag <image_prefix>/<image_name>:<image_tag> k8s.gcr.io/<image_name>:<image_tag>
mkdir ./kubeadm-images
docker save <image_name>.tar k8s.gcr.io/<image_name>:<image_tag>

scp -r ./kubeadm-basic.images root@192.168.66.11:/root/kubeadm-basic.images
```


开始初始化
```bash
# 打印默认初始化模版
kubeadm config print init-defaults > kubeadm-config.yaml

# 修改默认初始化模版
vim kubeadm-config.yaml
# 编辑...

# 初始化并设置为自动颁发证书
kubeadm init --config=kubeadm-config.yaml --experimental-upload-certs | tee kubeadm-init.log

# 查看日志并参考提示进行一些后续操作
cat kubeadm-init.log

# 后续操作
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

修改默认初始化模版
```yaml
apiVersion: kubeadm.k8s.io/v1beta2
bootstrapTokens:
- groups:
  - system:bootstrappers:kubeadm:default-node-token
  token: abcdef.0123456789abcdef
  ttl: 24h0m0s
  usages:
  - signing
  - authentication
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: 192.168.66.11 # 修改ip地址
  bindPort: 6443
nodeRegistration:
  criSocket: /var/run/dockershim.sock
  name: k8s-master01
  taints:
  - effect: NoSchedule
    key: node-role.kubernetes.io/master
---
apiServer:
  timeoutForControlPlane: 4m0s
apiVersion: kubeadm.k8s.io/v1beta2
certificatesDir: /etc/kubernetes/pki
clusterName: kubernetes
controllerManager: {}
dns:
  type: CoreDNS
etcd:
  local:
    dataDir: /var/lib/etcd
imageRepository: k8s.gcr.io
kind: ClusterConfiguration
kubernetesVersion: v1.15.1  # 修改版本
networking:
  dnsDomain: cluster.local
  podSubnet: "10.244.0.0/16"  # 默认Fluentd网段
  serviceSubnet: 10.96.0.0/12
scheduler: {}
--- # 默认调度方式改成ipvs调度方式
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
featureGates:
  SupportIPVSProxyMode: true
mode: ipvs
```

### 部署网络 (安装Flannel)
```bash
mkdir -p install-k8s/core
mv kubeadm-init.log kubeadm-config.yaml install-k8s/core

# 部署Flannel
# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

mkdir -p install-k8s/flannel
cd install-k8s/flannel
wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl create -f kube-flannel.yml
kubectl get pod -n kube-system # 查看pod kube-system命名空间状态
kubectl get node  # 查看node状态

# 复制join命令，将其他节点加入集群，命令可以从log文件从找到
# kubeadm join k8s-master01:6443 --token <token> ...

# 查看当前集群详细信息
kubectl get pod -n kube-system -o wide

# 监视节点状态
kubectl get pod -n kube-system -w
kubectl describe pod <coredns-5c98db65d4> -n kube-system

# 收尾
# 保存充要的安装文件以备后续使用
mv install-k8s/ /usr/local/

# 清理垃圾文件
rm -rf <files>
```

### Kubectl命令
```bash
# 根据资源清单创建pod
kubectl create -f <init-pod.yaml> --record

# 获取当前默认空间pod
kubectl get pod
# 等于
kubectl get pod -n default

# 查看默认命名空间pod详细信息
kubectl describe pod <pod-name>

# 查看容器日志
kubectl log <pod-name> -c <container-name>

# 删除所有的deployment
kubectl delete deployment --all

# 删除所有pod
kubectl delete pod --all

# 获取service
kubectl get svc

# 删除service
kubectl delete svc <service-name>

# 执行pod里面的命令
kubectl exec <pod-name> -it -- /bin/sh

# 获取replicaset
kubectl get rs

# 删除指定的rs
kubectl delete rs <rs-name>

# 为pod贴标签
kubectl label pod <pod-name> <label-name> --overwrite=True

# deployment
kubectl get deploy
kubectl delete deploy <deploy-name>
kubectl scale deploy <deploy-name> --replicas=5
kubectl set image deployment/<deploy-name> <container-name>=<image-name-v2>
kubectl rollout undo deployment/<deploy-name>
kubectl rollout status deployment/<deploy-name>
kubectl rollout pause deployment/<deploy-name>
kubectl rollout history deployment/<deploy-name>
kubectl rollout undo deployment/<deploy-name> --to-revision=2 # revision为指定版本

kubectl describe deployments

# job
kubectl get job
kubectl delete cronjob <job-name>
kubectl get cronjob
kubectl delete cronjob <cronjob-name>

# configmap
kubectl edit cm <cm-name>

# base64加解密
echo -n "admin" | base64
echo -n "YWRtaW4=" | base64 -d

# secret
kubectl create secret docker-registry labregistrykey --docker-server=hub.docker.com --docker-username=rayl --docker-password=xxxx --docker-email=rayl@pisaas.com
kubectl edit secret labregistrykey
```

## 相关
- 网络
  - [Flannel](https://github.com/coreos/flannel)
  - [Koolshare](https://github.com/koolshare)
    - [软路由搭建攻略：从小白到大白](https://post.smzdm.com/p/az59qdwo/)
    - [使用基于OpenWrt的软路由实现科学上网](https://github.com/ckjbug/Hacking/blob/master/VPS_VPN/OpenWrt2VPN.md)
- 日志收集工具
  - [Fluentd](https://github.com/fluent/fluentd)
  - [Logstach](https://github.com/elastic/logstash)
- 分布式存储
  - [Gluster](https://www.gluster.org/)
  - [Ceph](https://ceph.io/)
- 缓存代理服务器
  - [Squid](http://www.squid-cache.org/)
    - [Varnish 、 squid 和 nginx cache有什么不同？](http://www.360doc.com/content/18/0411/07/11935121_744636015.shtml)
    - [varnish、squid、nginx各自缓存的优缺点](https://blog.51cto.com/11859650/1911235)
    - [Nginx vs Squid](https://www.peterbe.com/plog/nginx-vs-squid)
    - [CDN实现方案如何选择: squid Varnish Nginx](https://www.cnblogs.com/atwanli/articles/5162589.html)
- Harbor
  - [Harbor官网](https://goharbor.io/)
  - [Harbor介绍及实践](https://cloud.tencent.com/developer/article/1404719)

## 疑点
- 建议高可用节点最好保存在3个以上的奇数，以便选举的时候确定选中一个
  - 有疑问，一个down了，不应该是2个，是偶数吗
- 目前Docker支持最好的Linux内核为4.4以上，3.X内会有一定意外重启风险，3.0以下内核需要重新编译。

## 参考
- [尚硅谷 Kubernetes最新最细视频教程](https://www.youtube.com/playlist?list=PLmOn9nNkQxJG2l1x9QgWkYSBg7VyjDj6o)
- [马哥Kubernetes](https://www.youtube.com/playlist?list=PLSmQArZ6iPY5QClDMGezJjEwU3B5CFAzU&pbjreload=10)
- [A Kubernetes guide for Docker Swarm lovers](https://hackernoon.com/a-kubernetes-guide-for-docker-swarm-users-c14c8aa266cc)
- [Kubernetes on a single machine](https://ubuntu.com/blog/kubernetes-on-a-single-machine)
- [Deploy a Single Node Kubernetes Instance in Seconds with MicroK8s](https://thenewstack.io/deploy-a-single-node-kubernetes-instance-in-seconds-with-microk8s/)
- [Kubernetes CNI网络最强对比：Flannel、Calico、Canal和Weave](http://dockone.io/article/8722)
- [K3s, minikube or microk8s?](https://www.reddit.com/r/kubernetes/comments/be0415/k3s_minikube_or_microk8s/)