# 使用Gitlab CI/CD

## 为什么使用Gitlab
我们前期使用bitbucket管理我们的源代码，后面也尝试过Coding，但在一段时间的使用后，基于以下原因，我们发现Gitlab或许是更好的选择：

- 由于项目在初期阶段，放在国内的源代码管理服务器上不放心，主要是怕代码泄漏
- 第二当然是Gitlab的免费，并且免费的基础上功能还很强大
- 很多平台(eg. Coding)的CI/CD支持或即将支持Gitlab
- Gitlab是唯一开源且免费提供本地安装的平台，这方便我们后期的直接迁移
- Gitlab的用户体验非常好，很类似于Github
- 最重要的一点是Gitlab的CI/CD做的比较好，简单易用，流程通俗易懂，Gitlab Runner为CI/CD的实现变得非常灵活


## 部署
考虑到种种原因，我们完全使用Docker进行开发和部署。如下是基本的部署步骤：
1. 在项目根目录下构建.gitlab-ci.yml并文件。
3. 在目标机器上部署Docker
2. 在目标机器上安装gitlab-runner

#### 创建.gitlab-ci.yml文件
.gitlab-ci.yml文件定义了CI如何工作，他需要位于源代码的根目录。具体可以参考： [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README.md)


#### 配置Gitlab Runner
Gitlab Runner是使用gitlab进行CI/CD的核心。


