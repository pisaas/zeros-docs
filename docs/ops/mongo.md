# Mongo运维

## mongo安装部署

### mongo 安装
添加mongo repo
```bash
sudo vim /etc/yum.repos.d/mongodb-org-3.6.repo
```
内容如下：
```
[mongodb-org-3.6]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.6/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.6.asc
```

安装
```bash
# 执行安装
sudo yum install -y mongodb-org
sudo service mongod start

# 查看安装日志
sudo cat /var/log/mongodb/mongod.log

mongo
```
设置记录操作日志
```bash
use <dbname>
db.getProfilingLevel()
db.setProfilingLevel(2)
db.getProfilingLevel()
db.getProfilingLevel()
db.system.profile.find().pretty()
```

### mongo 备份还原
备份
```bash
# 备份
mongodump -h 127.0.0.1 -d <dbname> -o ./<dbname>_$(date +%Y%m%d)

# 打包
tar zcvf <dbname>_$(date +%Y%m%d).tar.gz ./<dbname>_$(date +%Y%m%d)

# 上传
scp -r ./<dbname>_$(date +%Y%m%d).tar.gz <username>@<hostname>:/opt/apps/<dbname>/backups/db/<dbname>_$(date +%Y%m%d).tar.gz
```
还原
```bash
# 解包
tar zxvf <dbname>_$(date +%Y%m%d).tar.gz

# 备份原数据
mongodump -h 127.0.0.1 -d <dbname> -o ./<dbname>_$(date +%Y%m%d)_orig

# 还原
mongorestore -h 127.0.0.1:27017 -d <dbname> --drop ./<dbname>_$(date +%Y%m%d)/<dbname>
```
