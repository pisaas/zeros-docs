# Docker

经过调研，我们的CI/CD方案将构建在Docker上，以实现稳定性和弹性。接下来我们要做的就是建立Docker的开发和测试环境。

## Docker开发环境
由于我们主要在Mac上进行开发，因此选择安装 [Docker For Mac](https://docs.docker.com/docker-for-mac/), [下载地址](https://download.docker.com/mac/stable/Docker.dmg
)。

### Docker安装配置
Docker For Mac下载后，按提示安装，安装完成后，系统将会包括如下组件：Docker Engine, Docker CLI client, Docker Compose, Notary, Kubernetes, and Credential Helper。

由于国内Docker Hub的拉取速度实在崩溃，因此我们安装好Docker后，最好配置一下国内的Docker源。具体步骤如下：
1. 点击Docker图片->Perferences->Docker Engine
2. 填写如下配置，保存并重启：
```json
{
  "debug": true,
  "experimental": false,
  "registry-mirrors": [
    "http://ef017c13.m.daocloud.io",
    "https://registry.docker-cn.com"
  ]
}
```
3. 查看docker版本并尝试运行hello-world
```bash
# Linux上安装docker
# curl -sSL https://get.docker.com/ | sh

docker --version

docker run hello-world

# 查看images
docker images

# 运行nginx docker (启动后可以在本地访问localhost:9000)
docker run -d -p 9000:80 --name webserver nginx

docker container ls # 查看现在有的容器 (相当于docker ps)
docker container stop webserver # 停止Docker
docker container ls -a # 查看所有container，包括已停止的 (相当于docker ps -a)
docker container rm webserver # 删除container

docker image ls # 查看现在有的镜像
docker image rm nginx # 删除nginx镜像
docker rmi $(docker images --filter "dangling=true" -q) # 删除dangling镜像

docker container attach --help  # 进入运行状态的容器中
docker container exec --help # 进入处于运行状态的容器中，并执行命令

# 删除所有已停止的container
docker container rm `docker ps -a | grep Exited | awk   '{print $1}'`
```

### 新增命令自动补全
由于我们主要使用Zsh，在Zsh中要激活Docker命令自动补全，需要将这些文件复制或符号链接到Zsh的site-functions/目录。例如，如果你通过Homebrew安装Zsh:

```bash
etc=/Applications/Docker.app/Contents/Resources/etc
ln -s $etc/docker.zsh-completion /usr/local/share/zsh/site-functions/_docker
ln -s $etc/docker-machine.zsh-completion /usr/local/share/zsh/site-functions/_docker-machine
ln -s $etc/docker-compose.zsh-completion /usr/local/share/zsh/site-functions/_docker-compose
```

### 其他
- Try out the walkthrough at [Get Started](https://docs.docker.com/get-started/).
- Dig in deeper with [Docker Labs](https://github.com/docker/labs/) example walkthroughs and source code.
- For a summary of Docker command line interface (CLI) commands, see [Docker CLI Reference Guide](https://docs.docker.com/develop/sdk/).
- [Docker初始教程](https://www.runoob.com/docker/docker-tutorial.html)
- [Docker及Docker-Compose的使用](https://www.jianshu.com/p/ca1623ac7723)
- [Docker四种网络模式](https://www.jianshu.com/p/22a7032bb7bd)

## Docker部署（Centos 7.5）
由于我们目前配置Docker环境主要目的还是为了项目的快速迭代开发和推进，我们尽可能减少运维上的投入。Docker Swarm由于配置简单，并能满足我们在当前情况下的扩展性要求。我们将采用Docker Swarm进行部署。

### 部署工具
- Docker compose
  - 一般用于dev
  - 支持build、restart，但是不支持deploy
  - [官方文档](https://docs.docker.com/compose/)，[命令介绍及模版文件](https://www.cnblogs.com/minseo/p/11548177.html)，[参考案例](https://www.jianshu.com/nb/37195608)
- Docker stack/swarm
  - 一般用于多机
  - 运行时占用资源少
  - 支持deply的各种设置,包括分配cpu和内存，但是创建容器只支持从image，不支持build
  - [The Difference Between Docker Compose And Docker Stack](https://vsupalov.com/difference-docker-compose-and-docker-stack/)
  - [How to create Docker Swarm](https://www.youtube.com/watch?v=bU2NNFJ-UXA)
- Kubernetes
  - 分布式支持，扩展对象超过Docker swarm集群
  - 除Docker外还支持其他容器，比如Rocket
  - 比Swarm更稳定，可用性更高，但同时也更复杂
  - [Docker Swarm vs. Kubernetes: Comparison of the Two Giants in Container Orchestration](https://upcloud.com/community/stories/docker-swarm-vs-kubernetes-comparison-of-the-two-giants-in-container-orchestration/?utm_term=&utm_campaign=DSA&utm_source=adwords&utm_medium=ppc&hsa_acc=9391663435&hsa_cam=7185608860&hsa_grp=80829237094&hsa_ad=394414806198&hsa_src=g&hsa_tgt=dsa-376606137543&hsa_kw=&hsa_mt=b&hsa_net=adwords&hsa_ver=3&gclid=EAIaIQobChMIs8fU74Pu5wIVU7eWCh2Yzg2WEAAYASAAEgJhUPD_BwE)
  - [''Docker Swarm or Kubernetes?'': Is It the Right Question to Ask?](https://dzone.com/articles/quotdocker-swarm-or-kubernetesquot-is-it-the-right)

## 参考

### 常用
检查Linux版本以及包管理工具（eg. /etc/apk, /etc/apt, /etc/yum）
```bash
# for Debian, CentOS and Alpine
cat /etc/os-release

# for Ubuntu
cat /etc/lsb-release
```

