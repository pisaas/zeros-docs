# Ansible
由于目前项目并不复杂。运维操作暂时直接通过ssh完成。这里内容暂作参考。

```bash
#!/bin/bash

# 参考：https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-pip

sudo easy_install pip
sudo pip install ansible
ansible --version

sudo mkdir /etc/ansible

sudo vim /etc/ansible/hosts
# 输入如下内容
## [pi]
## pi1 ansible_ssh_host=123.206.208.176 ansible_ssh_user=admin

## [ydpm]
## ydpm5 ansible_ssh_host=43.226.16.158 ansible_ssh_port=22122 ansible_ssh_user=jk_admin

# 测试
ansible all -m ping -vvv
ansible all -a "echo hello"

# 调试
ansible all -m ping -vvv

# 服务mongod在ydp组中启动
ansible ydpm -m service -a "name=mongod state=started"

```