# redis运维

## redis安装部署

### 手工安装
```
cd /opt/pkgs
wget http://download.redis.io/releases/redis-3.2.6.tar.gz

tar xzf redis-3.2.6.tar.gz
cd redis-3.2.6
make
make test
sudo make install
cd utils
chmod +x install_server.sh

mkdir -p /opt/ops/redis

./install_server.sh

# 提示及相应参数
# Please select the redis port for this instance: # [6379]
# Selecting default: 6379
# Please select the redis config file name [/etc/# redis/6379.conf] /opt/ops/redis/etc/6379.conf
# Please select the redis log file name [/var/log/# redis_6379.log] /opt/ops/redis/log/redis_6379.log
# Please select the data directory for this instance # [/var/lib/redis/6379] /opt/ops/redis/lib/6379
# Please select the redis executable path [/usr/local/# bin/redis-server] /opt/ops/redis/bin/redis-server

# Selected config:
# Port           : 6379
# Config file    : /opt/ops/redis/etc/6379.conf
# Log file       : /opt/ops/redis/log/redis_6379.log
# Data dir       : /opt/ops/redis/lib/6379
# Executable     : /usr/local/bin/redis-server
# Cli Executable : /usr/local/bin/redis-cli

sudo chkconfig --level 2345 redis_6379 on

service redis_6379 status
service redis_6379 start
```


#### 移除appnode相关包
```
sudo yum remove appnode*
```
 
## Redis性能调试

### redis-benchmark
redis基准信息，redis服务器性能检测
```
// 100个并发连接，100000个请求，检测host为localhost 端口为6379的redis服务器性能
redis-benchmark -h localhost -p 6379 -c 100 -n 100000
```

### redis-cli
```
// 监控host为localhost，端口为6379，redis的连接及读写操作
redis-cli -h localhost -p 6379 monitor
```
```
// 提供host为localhost，端口为6379，redis服务的统计信息
redis-cli -h localhost -p 6379 info
```

### redis-stat
```
// 实时打印出host为localhost，端口为6379，redis实例的总体信息
redis-stat host localhost port 6379 overview

// 输出host为localhost，端口为6379，redis服务中每个请求的响应时长
redis-stat host localhost port 6379 latency
```

### showlog功能
Slowlog 将会记录运行时间超过Y微秒的最后X条查询. X 和 Y 可以在 redis.conf 或者在运行时通过 CONFIG 命令:
```
// 设置将记录执行时间超过5秒的查询
CONFIG SET slowlog-log-slower-than 5000
CONFIG SET slowlog-max-len 25

// 获取10条记录的日志
SLOWLOG GET 10
```
### 统计各种数据大小
Redis 内存比较大的话，我们就不太容易查出是哪些（种）键占用的空间。有一些工具能够提供必要的帮助，比如 redis-rdb-tools 可以直接分析 RDB 文件来生成报告
