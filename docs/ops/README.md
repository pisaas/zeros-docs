# 应用部署

## zeros部署策略
为了提高开发部署效率，Zeros采用继续集成和部署的方式进行开发和验证工作成果。同时由于Zeros目前还在初步开发阶段，因此部分代码不适宜公开，因此在持续集成的过程中需要兼顾代码的安全。在保证以上要求的前提下要考虑到成本和效率。

## 架构
- CI/CD
  - Docker Swarm
    - 容器自带集群支持，部署相对简单，资源消耗小，维护成本低，适合中小规模使用
  - Kubernetes
    - 部署复杂，资源消耗大，维护成本高，适合中大规模使用
- API Gateway
  - Kong：
    - 先已官方支持 Let's Encrypt
      - [kong-plugin-acme](https://github.com/Kong/kong-plugin-acme)
    - 广泛使用，以及支持插件
    - 设计为API Gateway，功能强大
      - [使用kong作为docker swarm的集群网关](https://cloud.tencent.com/developer/article/1472613)
      - [开源API网关系统（Kong教程）入门到精通](https://www.jianshu.com/p/a68e45bcadb6)
      - [Kubernetes Ingress Compared To Docker Swarm Equivalent](https://technologyconversations.com/2017/12/26/kubernetes-ingress-compared-to-docker-swarm-equivalent/)
    - 支持GraphQL
    - 需要安装额外数据库 （postgres, cassendra）
    - 支持Ingress
  - Traefik：
    - 集成 Let's Encrypt
    - 天生支持容器技术 (如：Ingress)
    - 不支持插件，扩展困难
    - UI只支持监控，不支持管理

## 部署方案选型
在综合以上条件后，我们分析了如下机制持续集成方案。
- ssh进行CI和CD
- Docker上进行CI，ssh进行CD
- Docker上CI和CD

### ssh进行CI和CD
ssh进行CI和CD流程为：
1. 提交代码后，直接拉取代码安装依赖并执行测试代码
2. 代码测试完成后，按一定步骤直接重启服务器生效

#### Pros
- 简单直接高效，初期时可以选择此方案

#### Cons
- 部署过程中，特别是执行测试时，可能需要某些新建特定的用户或特权，这有可能会对本机其他应用运行产生影响

### Docker上进行CI，ssh进行CD
为了减少测试过程对其他应用的影响，CI过程采用Docker。
CI/CD的流程为：
1. 提交代码后，开始构建CI Docker
2. CI Docker拉取最新代码，安装依赖并执行测试代码
3. 测试通过后开始，按一定步骤直接重启服务器生效

#### Pros
- 减少测试过程对其他应用的影响

#### Cons
- 配置相对方案1稍微有些麻烦
- 不能完全隔离不同应用执行

### Docker上CI和CD
为了更方便的维护Docker以及保证可用性，可以考虑建立Kubernetes集群。
CI/CD的流程为：
1. 提交代码后，开始构建CI Docker
2. CI Docker拉取最新代码，安装依赖并执行测试代码
3. 测试通过后开始构建应用Docker
4. 运行Docker，流程结束

#### Pros
- 代码在Docker Container中运行，整体环境保持相对独立，不影响当前机器其他应用运行，弹性十足。
- 项目后期，可以安装Kubernetes集群时，整个方案可以平稳进行过渡。

#### Cons
- 配置执行相对略显麻烦，比如宿主机器需要安装Docker环境，并需要在Docker中配置代码卷以及npm缓存卷路径等。
- Kubernete集群成本略高，在产品还在开发初期，基础设施和与关键任务无关的人力投入应当精简。

## 结论
在分析了众多利弊，我们最终觉得采用方案3。原因有：
- 我们当前的服务器已经运行了多个不同项目的应用。这些项目虽然采用同一套架构，不同的项目升级更新的升级更新可能会对地址支持产生不同的需求，比如需要特定特定版本的Node.js等。
- 另外作为目标是成熟应用的我们，不同应用之间的隔离还是有必要的。